from django.http import HttpResponse
from django.shortcuts import redirect
from django.core.mail import send_mail


def index_view(request):
    msg = str(request)
    send_mail('Click registered', msg, 'noreply@zet.pythonanywhere.com', ['zet-00@mail.ru'], fail_silently=False)

    if 'next' in request.GET:
        return redirect(request.GET['next'])
    else:
        return redirect('http://hh.ru/search/vacancy?area=1743&text=Вытегра')

    # return HttpResponse(msg)
